<h1 align="center">Welcome to recruit-wx 👋</h1>
<p>
  <img alt="Version" src="https://img.shields.io/badge/version-1.0.0-blue.svg?cacheSeconds=2592000" />
  <img src="https://img.shields.io/badge/npm-%3E%3D5.5.0-blue.svg" />
  <img src="https://img.shields.io/badge/node-%3E%3D9.3.0-blue.svg" />
</p>

> uniapp+unicloud+colorui+uview招聘小程序，开箱即用，包含招聘者和应聘者角色

## Prerequisites

- npm >=5.5.0
- node >=9.3.0

## configuration
#### 1.导入HBuilder X，右键uniCloud-aliyun --> 云服务空间初始化向导

#### 2.配置：uniCloud-aliyun\cloudfunctions\common\wx-common\index.js

``` js
const appId = "自己的小程序appid" //微信小程序
const appSecret = "自己的小程序appSecret" //微信小程序
const h5AppId = "自己的公众号h5AppId" //微信公众号
const h5AppSecret = "自己的公众号h5AppSecret" //微信公众号
```

#### Show Pages

  <img alt="Version" src="./static/shows/show1.jpg" width="240"/>
  <img alt="Version" src="./static/shows/show2.jpg" width="240"/>
  <img alt="Version" src="./static/shows/show3.jpg" width="240"/>
  <img alt="Version" src="./static/shows/show4.jpg" width="240"/>
  <img alt="Version" src="./static/shows/show5.jpg" width="240"/>
  <img alt="Version" src="./static/shows/show6.jpg" width="240"/>
  <img alt="Version" src="./static/shows/show7.jpg" width="240"/>
  <img alt="Version" src="./static/shows/show8.jpg" width="240"/>
  <img alt="Version" src="./static/shows/show9.jpg" width="240"/>
  <img alt="Version" src="./static/shows/show10.jpg" width="240"/>
  <img alt="Version" src="./static/shows/show11.jpg" width="240"/>
  <img alt="Version" src="./static/shows/show12.jpg" width="240"/>
  <img alt="Version" src="./static/shows/show13.jpg" width="240"/>
  <img alt="Version" src="./static/shows/show14.jpg" width="240"/>
  <img alt="Version" src="./static/shows/show15.png" width="480"/>
  <img alt="Version" src="./static/shows/show16.png" width="480"/>